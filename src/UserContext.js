import React from 'react';

// Create a Context object 

// Context obecj is a different approach in passing information between components and allows easier by avoiding props - drilling

// type of object that can be used to store information that can be shared to other comonenxts withinc the app.

const UserContext = React.createContext(); // Base

// The "Provider" component allows other components to consume/use the context object and supply the neccessary information need to the context objects.


export const UserProvider = UserContext.Provider


export default UserContext //Deliver