import './App.css';
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'
import Home from './pages/Home.js'
import Product from './pages/Products.js'
import AdminProducts from './pages/AdminProducts.js'
import ProductView from './components/ProductView'
import Login from './pages/Login.js'
import Register from './pages/Register'
import CreateProduct from './pages/CreateProduct'
import AppNavbar from './components/Navbar.js'
import Logout from './pages/Logout.js'
import Footer from './components/Footer.js'
import Edit from './components/AdminProductview.js'
import Orders from './pages/Orders.js'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {

    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

      //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
      useEffect(() => {

      fetch(`${ process.env.REACT_APP_API_URL }/users/getuser`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {

        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

        // Else set the user states to the initial values
        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

      }, []);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
      <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/login" element={<Login/>}/>
      <Route path="/register" element={<Register/>}/>
      <Route path="/logout" element={<Logout/>}/>
      <Route path="/products" element={<Product/>}/>
      <Route path="/products/:productId" element={<ProductView/>}/>
      <Route path="/products/addProducts" element={<CreateProduct/>}/>
      <Route path="/adminproducts" element={<AdminProducts/>}/>
      <Route path="/adminproducts/edit/:productId" element={<Edit/>}/>
      <Route path="/orders" element={<Orders/>}/>
      </Routes>

    </Router>
    </UserProvider>
    <Footer />
    </>
    

  );
}

export default App;
