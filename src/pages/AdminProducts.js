import ProductCard from '../components/ProductCard.js'
import {Container} from 'react-bootstrap'
import Loading from '../components/Loading.js'
import {useContext, useEffect, useState} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'


export default function Products(){

	const {user} = useContext(UserContext)

	const [products, setProducts] = useState([])

	const [isLoading, setIsLoading] = useState(true)

	useEffect((isLoading) => {

		// If it detects the data coming from fetch, the set isloading going to be false
		fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`, {
			method : 'POST',
			headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setProducts(
				result.map(product => {
					return (
						<ProductCard key={product._id} productProp ={product}/>
					)
				})
			)

			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])



	return(
			(!user.isAdmin) ?
				<Navigate to="/products" />
				:
				(isLoading ?
					<Loading/>
				:
				<>
					<Container className="productContainer" id="product--container">
					{products}
					</Container>
				</>)

				
	)
}