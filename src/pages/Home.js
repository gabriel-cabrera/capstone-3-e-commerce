import Banner from '../components/Banner.js';
import Carousel from '../components/HomeCarousel.js'
import Footer from '../components/Footer.js'
import Container from 'react-bootstrap';



export default function Home() {
    return(
        <>
                <Banner />
                <Carousel />
        </>
    )
}