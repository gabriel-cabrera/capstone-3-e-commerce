import {useState, useEffect, useContext} from 'react'
import { Form, Button } from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const [isActive, setIsActive] = useState(false)

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/getuser`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {


            // Store the user details retrieved from the token into the global user state
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

	function authenticate(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.access !== "undefined"){
				localStorage.setItem('token', result.access)

				retrieveUser(result.access)

				Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Damit Co.'
                })
            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Invalid Email or password'
                })
			}
		})
	}

// Enables the submit button if the form data has been verified
	useEffect(() => {
		if((email !=='' && password !== '')){

			setIsActive(true)
		}
	})

	return (

		user.id === null ?
		<Form className="form--login" onSubmit={event => authenticate(event)}>
      <Form.Group className="mb-3 login--input" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value)} required/>
        <Form.Text className="text-muted formSubs">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3 login--input" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
      </Form.Group>
      {/*<Form.Group className="mb-3 input--register" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Keep me logged in" />
      </Form.Group>*/}
      { isActive ?
      <Button variant="primary" type="submit">
        Submit
      </Button>
      :
      <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
      }
    </Form>
			:
	(user.isAdmin ? 
		<Navigate to="/" /> 
		: 
		<Navigate to="/" />)

		)
}