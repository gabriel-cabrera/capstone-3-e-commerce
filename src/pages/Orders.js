import OrderAccordion from '../components/OrderAccordion.js'
import {useEffect, useState, useContext} from 'react'
import Loading from '../components/Loading.js'
import {Container} from 'react-bootstrap'
import UserContext from '../UserContext'

export default function Orders(){
	const [orders, setOrders] = useState([])
	const [isLoading, setIsLoading] = useState(true)
	const {user, setUser} = useContext(UserContext)

	useEffect((isLoading) => {

		(user.isAdmin) ?
		fetch(`${process.env.REACT_APP_API_URL}/orders/getAllOrders`, {
			method : 'POST',
			headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
          })
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setOrders(
				result.map(order => {
					return (
						<OrderAccordion key={order._id} orderProp ={order}/>
					)
				})
			)

			// Sets the loading state to false
			setIsLoading(false)
		})
	
		:
		fetch(`${process.env.REACT_APP_API_URL}/orders/getUserOrders`, {
			headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setOrders(
				result.map(order => {
					return (
						<OrderAccordion key={order._id} orderProp ={order}/>
					)
				})
			)

			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return (

			(isLoading) ?
				<Loading /> 
		:
			<>
				<Container>
				{orders}
				</Container>
			</>
		)
}