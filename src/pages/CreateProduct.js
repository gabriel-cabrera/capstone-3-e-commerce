import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function CreateProduct() {

  const {user, setUser} = useContext(UserContext)
  const navigate = useNavigate()

  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState('')
  const [image, setImage] = useState('')

  const [isActive, setIsActive] = useState(false)

  function createProduct(event){
    event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addproduct`, {
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            name : name,
            description: description,
            price:price,
            image: image
          })
        })
        .then(response => response.json())
        .then(result => {
          console.log(result);
          
          setName('');
          setDescription('');
          setPrice('');
          setImage('');

          if(result){
            Swal.fire({
              title: 'Product Created!',
              icon: 'success',
              text: 'Product has been added'
              })
            
            navigate('/products/addProducts')

          } else {
            Swal.fire({ 
              title: 'Creation Failed',
              icon: 'error',
              text: "Please check details provided"
            })
          }   
        })
      }
    
 

  useEffect(() => {
    if(name !== '' && price !== '' && description !== '' && image !== '' ){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [name, price, description, image])

  return (
    (user.id !== null && !user.isAdmin) ?
    <Navigate to ="/" />
    :
    <Form className="form--create" onSubmit={event => createProduct(event)}>

      <Form.Group className="mb-3 create--input" controlId="formBasicFirstName" >
        <Form.Label>Product Name</Form.Label>
        <Form.Control type="text" placeholder="Enter product name" value={name} onChange={event => setName(event.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3 create--input" controlId="formBasicLastName">
        <Form.Label>Price</Form.Label>
        <Form.Control type="text" placeholder="Enter product price" value={price} onChange={event => setPrice(event.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3 create--input" controlId="formImage">
        <Form.Label>Image</Form.Label>
        <Form.Control type="text" placeholder="Enter Image URL" value={image} onChange={event => setImage(event.target.value)} required/>
      </Form.Group>

        <Form.Group className="mb-3 create--description" controlId="formBasicEmail">
        <Form.Label className="create--descriptionText">Description</Form.Label>
        <textarea className="create--textArea" type="text" placeholder="Enter product description" value={description} onChange={event => setDescription(event.target.value)} required/>
      </Form.Group>


      { isActive ?
      <Button variant="primary" type="submit">
        Submit
      </Button>
      :
      <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                  </Button>
      }
    </Form>

    )
}