import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {

  const {user, setUser} = useContext(UserContext)
  const navigate = useNavigate()

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')

  const [isActive, setIsActive] = useState(false)

  function registerUser(event){
    event.preventDefault()

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method : 'POST',
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        email:email
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result === true){
        Swal.fire({
          title: 'Oops!',
          icon: 'error',
          text: 'Email already exist!'
        })
      } else {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email:email,
            password: password1
          })
        })
        .then(response => response.json())
        .then(result => {
          console.log(result);
          
          setEmail('');
          setPassword1('');
          setPassword2('');
          setFirstName('');
          setLastName('');

          if(result){
            Swal.fire({
              title: 'Register Successful!',
              icon: 'success',
              text: 'Account has been created'
              })
            
            navigate('/login')

          } else {
            Swal.fire({ 
              title: 'Registration Failed',
              icon: 'error',
              text: "Please check details provided"
            })
          }   
        })
      }
    })
  }

  useEffect(() => {
    if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [firstName, lastName, email, password1, password2])

  return (
    (user.id !== null) ?
    <Navigate to ="/" />
    :
    <Form className="form--register" onSubmit={event => registerUser(event)}>
      <Form.Group className="mb-3 input--register" controlId="formBasicFirstName" >
        <Form.Label>First Name</Form.Label>
        <Form.Control type="firstName" placeholder="Enter first name" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3 input--register" controlId="formBasicLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="lastName" placeholder="Enter last name" value={lastName} onChange={event => setLastName(event.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3 input--register" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="Enter email address" value={email} onChange={event => setEmail(event.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3 input--register" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
      </Form.Group>
      <Form.Group className="mb-3 input--register" controlId="formBasicPassword2">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control type="password" placeholder="Confirm password" value={password2} onChange={event => setPassword2(event.target.value)} required/>
      </Form.Group>

      { isActive ?
      <Button variant="primary" type="submit">
        Submit
      </Button>
      :
      <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                  </Button>
      }
    </Form>

    )
}