import Carousel from 'react-bootstrap/Carousel';

function HomeCarousel() {
  return (
    <Carousel>
      <Carousel.Item interval={10000}>
        <img
          className="d-block w-100"
          src="http://blog.creativelive.com/wp-content/uploads/2015/04/Blair_Stocker-.jpg"
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={10000}>
        <img
          className="d-block w-100"
          src="http://gogreenthriftstore.com/59/wp-content/uploads/2021/02/Bryans-Road-Interior_1640x781-1024x488.jpg"
          alt="Second slide"
        />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.uncovercolorado.com/wp-content/uploads/2022/05/2nd-time-around-thrift-clothing-colorado-1600x800.jpeg"
          alt="Third slide"
        />
        <Carousel.Caption>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default HomeCarousel;