import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg, Form } from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductView(product) {

	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");
	const [isActive, setIsActive] = useState(false)

	function updateProduct(event){
    event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${productId}`, {
          method: 'PUT',
          headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            name : name,
            description: description,
            price:price
          })
        })
        .then(response => response.json())
        .then(result => {
          console.log(result);
          
          setName('');
          setDescription('');
          setPrice('');

          if(result){
            Swal.fire({
              title: 'Product Updated!',
              icon: 'success',
              text: 'Product has been updated!'
              })
            
            navigate(`/adminproducts`)

          } else {
            Swal.fire({ 
              title: 'Update Failed',
              icon: 'error',
              text: "Please check details provided"
            })
          }   
        })
      }
    
 

  useEffect(() => {
    if(name !== '' && price !== '' && description !== ''){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [name, price, description])

  useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			console.log(result.name)
			console.log(result.price)
			console.log(result.description)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [productId])

  return (
    (user.id !== null && !user.isAdmin) ?
    <Navigate to ="/" />
    :
    <Form className="form--edit" onSubmit={event => updateProduct(event)}>

      <Form.Group className="mb-3 edit--name" controlId="formBasicFirstName" >
        <Form.Label>Product Name:</Form.Label>
        <Form.Control type="text" placeholder="Enter product name" value={name} onChange={event => setName(event.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3 edit--name" controlId="formBasicLastName">
        <Form.Label>Price:</Form.Label>
        <Form.Control type="text" placeholder="Enter product price" value={price} onChange={event => setPrice(event.target.value)} required/>
      </Form.Group>

        <Form.Group className="mb-3 edit--description" controlId="formBasicEmail">
        <Form.Label className="edit--descriptionText">Description:</Form.Label>
        <textarea className="edit--textArea" type="text" placeholder="Enter product description" value={description} onChange={event => setDescription(event.target.value)} required/>
      </Form.Group>


      { isActive ?
      <Button variant="primary" type="submit">
        Submit
      </Button>
      :
      <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                  </Button>
      }
    </Form>

    )
}