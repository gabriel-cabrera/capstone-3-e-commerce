import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown';
import Badge from 'react-bootstrap/Badge';
import Container from 'react-bootstrap/Container';
import {Link, NavLink} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext, useEffect, useState} from 'react'


export default function AppNavbar(){

	const {user} = useContext(UserContext)

	return(
		<Navbar className="navbar--main" collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container id="navbar--container">
        <Navbar.Brand as={Link} to="/">Damit Co.</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto nav--default">
            <Nav.Link as={Link} to="/" className="nav--links">Home</Nav.Link>
            {user.isAdmin ?
              <>
              <Nav.Link as={Link} to="/adminproducts" className="nav--links">Products</Nav.Link>
              <Nav.Link as={Link} to="/products/addProducts" className="nav--links">Add Product</Nav.Link>
              <Badge className="badge--main" bg="success">
                ADMIN
              </Badge>{' '}
              </>
              :
              <Nav.Link as={Link} to="/products" className="nav--links">Product</Nav.Link>
            }
          </Nav> 
          <Nav className="nav--default">
          {(user.id !== null) ?
            <>
            <Nav.Link  as={Link} to="/orders" className="nav--links">
              Orders
            </Nav.Link>
            <Nav.Link  as={Link} to="/logout" className="nav--links">
              Logout
            </Nav.Link>
            </>
            :
            <>
            <Nav.Link as={Link} to="/login" className="nav--links">Login</Nav.Link>
            <Nav.Link  as={Link} to="/register" className="nav--links">
              Register
            </Nav.Link>
            </>
            }
          </Nav>
      	
        </Navbar.Collapse>
      </Container>
    </Navbar>

		);
}

