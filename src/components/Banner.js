import { Row, Col } from 'react-bootstrap';

export default function Banner() {
	return (
		<Row id="bannerRow">
			<Col id="bannerCol">
				<h1 id="banner">Damit Co.</h1>
				<p>Thrift shopping made online for you</p>
				
			</Col>
		</Row>
	)
}