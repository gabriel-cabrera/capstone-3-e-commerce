import { useState, useEffect, useContext, setState } from 'react';
import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
import CheckOut from './CheckOut.js'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductView(product) {

	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");
	const [status, setStatus] = useState();


		useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			console.log(result.name)
			console.log(result.price)
			console.log(result.description)
			console.log(result.isActive)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setStatus(result.isActive)
		})
	}, [productId])


	const archive = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: status
			})
		})
		.then(response => response.json())
		.then(result => {

			setStatus(false);

			if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Status has been updated!"
				})

				navigate('/adminproducts')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
		})
	}

	const unarchive = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/products/unarchive/${productId}`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: status
				})
			})
			.then(response => response.json())
			.then(result => {

				setStatus(true);

				if(result) {
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "Status has been updated!"
				})

				navigate('/adminproducts')
			} else {
				console.log(result)

				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again :("
				})
			}
			})
		}

	return(
	<>
		<Container className="mt-2 productV--container">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="card--body">
						<Card.Body className="productV--card text-center">
							<Card.Title className="productV--title">{name}</Card.Title>
							<Card.Text className="productV--text productV--description">{description}</Card.Text>
							<Card.Text className="productV--text">PhP {price}</Card.Text>
							{(user.isAdmin) ?
								<Card.Text>Status: {status ? "Active" : 'Inactive'}</Card.Text>
								:
								<CardImg top src={image}/>
							}
							


							{	user.id === null ? 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Purchase</Link>
								:

									(user.isAdmin)?
									<>
									<Link className="btn btn-primary btn-block productV--edit" to={`/adminproducts/edit/${productId}`}>Edit</Link>
									{(status) ?
									<Button className="productV--button" variant="danger" onClick={() => archive(productId)}>Archive</Button>
									:
									<Button className="productV--button" variant="primary" onClick={() => unarchive(productId)}>Unarchive</Button>}
									</>
									:
									<CheckOut />
									


							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	</>
	)
}