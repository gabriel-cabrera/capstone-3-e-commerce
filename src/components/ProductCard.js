import {Card, CardImg} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'

export default function ProductCard(product){

  const {user} = useContext(UserContext)
  // Destructuring the props
  const {name, description, price, isActive, _id, image} = product.productProp

  return(
    <>
    <Card className="cardContainer">
      <Card.Body className="card--main">
        <Card.Title className="card--title">{name}</Card.Title>
        <Card.Text className="cardText cardDescription">{description}</Card.Text>
        <Card.Text className="cardText">PHP {price}</Card.Text>
        <CardImg className="productImage" top src={image}/>
        {(user.isAdmin) ?
        <>
          <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
          <Card.Subtitle className="productStatus">Status: {isActive ? "Active" : 'Inactive'}</Card.Subtitle>
        </>
          :
        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
        }
      </Card.Body>
    </Card>
    </>
  )
}