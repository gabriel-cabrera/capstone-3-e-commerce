import React from 'react';
import {
  MDBFooter,
} from 'mdb-react-ui-kit';

export default function Footer() {
  return (
    <MDBFooter  className='text-center fixed-bottom footer--main' color='white' bgColor='dark' variant='dark'>
    <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2023 Copyright GNRC
      </div>
    </MDBFooter>
    )
  }