import Accordion from 'react-bootstrap/Accordion';
import UserContext from '../UserContext'
import {useContext} from 'react'

function OrderAccordion(order) {

  const {user, setUser} = useContext(UserContext)


  const {_id, userId, products, totalAmount, purchasedOn} = order.orderProp

  return (
    user.isAdmin ? 
    <>
    <Accordion defaultActiveKey="0">
      <Accordion.Item eventKey="1">
        <Accordion.Header>Purchase Order# {_id}</Accordion.Header>
        <Accordion.Body>
         {products.map(product => 
          <>
          <div>User ID: {userId}</div>
          <div>Product ID: {product.productId}</div>
          <div>{product.name}</div>
          <div>Price: {product.price}</div>
          </>
          )}
         <div>Total Amount : {totalAmount}</div>
         <div>Purchased on: {purchasedOn}</div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
    </>
    :
    <>
    <Accordion defaultActiveKey="0">
      <Accordion.Item eventKey="1">
        <Accordion.Header>Purchase Order# {_id}</Accordion.Header>
        <Accordion.Body>
         {products.map(product => 
          <>
          <div>Product ID: {product.productId}</div>
          <div>{product.name}</div>
          <div>Price: {product.price}</div>
          </>
          )}
         <div>Total Amount : {totalAmount}</div>
         <div>Purchased on: {purchasedOn}</div>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
    </>
  );
}

export default OrderAccordion;